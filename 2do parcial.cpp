#include <iostream>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <cstring>
using namespace std;
int main()
{
    string cadena1="";

    std::cout << "Ingrese la primer cadena: ";
    getline(cin,cadena1);
    std::cout << "Ingrese la cadena 2: ";
    string cadena2;
    getline(cin,cadena2);

    std::cout << "La longitud de las cadenas 1 y 2 son: "<<cadena1.length()<<" "<<cadena2.length()<<endl;
    std::cout << "Las cadenas concatenadas son: " << cadena1<<cadena2<< endl;
    const int tam1 = cadena1.length();

    char* cadena1Char;
    cadena1Char = &cadena1[0];

    char* cadena2Char;
    cadena2Char = &cadena2[0];

    std::cout << "Las cadenas intercaladas son: ";
    std::cout << cadena2Char[0];
    for (int i = 1; i < cadena1.length(); i++) {
        std::cout<<cadena1Char[i];
    }
    std::cout << " ";
    std::cout << cadena1[0];
    for (int i = 1; i < cadena2.length(); i++) {
        std::cout << cadena2Char[i];
    }
}

